# godot-mumble-link

A gdnative library for godot to use mumble's positional audio feature

## Usage

Build the rust crate, then create a GDNativeLibrary resource from `target/release/libgodot_mumble_link.so`.
Once attached to a node, the following functions can be used:

Function signature|Description
---|---
`void set_context(namespace: String)`|
`void set_identity(nickname: String)`|
`void deactivate()`|
`void update_position(position: Vector3, front: Vector3, up: Vector3)` | this sets camera and player position to the same value
`void update_position_camera(player_position: Vector3, player_front: Vector3, player_up: Vector3, camera_position: Vector3, camera_front: Vector3, camera_up: Vector3)`|

## License

GNU Affero General Public License version 3 only.
