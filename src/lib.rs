use gdnative::prelude::*;
use mumble_link::{Position, SharedLink};

#[derive(NativeClass)]
#[inherit(Node)]
pub struct MumbleLink {
    link: SharedLink,
}

#[methods]
impl MumbleLink {
    fn new(_owner: &Node) -> Self {
        MumbleLink {
            link: SharedLink::new("libgodot_mumble_link", ""),
        }
    }

    #[export]
    fn set_context(&mut self, _owner: &Node, namespace: String) {
        self.link.set_context(namespace.as_bytes())
    }

    #[export]
    fn set_identity(&mut self, _owner: &Node, nickname: String) {
        self.link.set_identity(nickname.as_str())
    }

    #[export]
    fn deactivate(&mut self, _owner: &Node) {
        self.link.deactivate()
    }

    #[export]
    fn update_position(&mut self, _owner: &Node, position: Vector3, front: Vector3, up: Vector3) {
        let p = Position {
            position: [position.x, position.y, position.z],
            front: [front.x, front.y, front.z],
            top: [up.x, up.y, up.z],
        };
        self.link.update(p, p);
    }

    #[export]
    fn update_position_camera(
        &mut self,
        _owner: &Node,
        player_position: Vector3,
        player_front: Vector3,
        player_up: Vector3,
        camera_position: Vector3,
        camera_front: Vector3,
        camera_up: Vector3,
    ) {
        self.link.update(
            Position {
                position: [player_position.x, player_position.y, player_position.z],
                front: [player_front.x, player_front.y, player_front.z],
                top: [player_up.x, player_up.y, player_up.z],
            },
            Position {
                position: [camera_position.x, camera_position.y, camera_position.z],
                front: [camera_front.x, camera_front.y, camera_front.z],
                top: [camera_up.x, camera_up.y, camera_up.z],
            },
        );
    }
}

fn init(handle: InitHandle) {
    handle.add_class::<MumbleLink>();
}

godot_init!(init);
