extends KinematicBody2D


func _ready():
	self.owner.set_identity("ingame username here")

func _physics_process(_delta):
	var pos = Vector3(self.global_position.x / -2, self.global_position.y / 2, 1.0);
	self.owner.update_position(pos, Vector3.FORWARD, Vector3.UP)
